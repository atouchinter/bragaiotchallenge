![Logo](https://bitbucket.org/atouchinter/bragaiotchallenge/raw/98a761aefab549cbc62ada741b5a56d47baae101/imagens/Logo.jpg)

A Atouch Winwel através de um Projecto de Investigação e Desenvolvimento Tecnológico DONA – DOMOTICS NEW APPLICATIONS (financiado no âmbito do Portugal 2020) desenvolveu um sistema de domótica inteligente e inovador, uma atividade de forte intensidade tecnológica e de conhecimento, contribuindo para o objetivo de aumentar o investimento em I&D e a criação de valor baseado na inovação. Assim, o sistema de domótica desenvolvido que permite controlar a casa à distância e criar automatismos que melhorem o conforto, bem-estar, gestão de recursos dos utilizadores e segurança. Um dos focos deste projeto é igualmente a criação de um sistema de domótica Wifi (DONA WIFI) compatível com a maioria dos aparelhos e dispositivos convencionais, procurando também desta forma democratizar o acesso à domótica e ao usufruir de casas inteligentes.

O objectivo do Braga IoT Challenge 2021 passa por:

– Promover e divulgar o esforço de desenvolvimento nas empresas portuguesas em particular no projecto SI IDT 2020;
– A interação e envolvimento da academia, o mais cedo possível, no ambiente profissional para benefício de todas as partes envolvidas.

Desta forma, lança-se o desafio ao desenvolvimento de um produto, via Firmware ou Hardware, baseado no trabalho executado durante o projeto SI IDT. As áreas de atuação que serão consideradas com majoração na avaliação são:

a) Domótica;
b) Indústria;
c) Alimentar;
d) Ambiente.

Outras áreas poderão ser submetidas, mas sem majoração, ou seja, terão de ter um mérito próprio elevado que irá ser contemplado pelo júri conforme a sua interpretação (não definida ou enquadrada neste documento).

Mais informações acerca do Braga IOT challenge pode ser consultada em [2020.winwel.pt]().



# Kit

O kit de desenvolvimento é constituído por três módulos, um modulo wifi estores, um modulo wifi tomada, um modulo wifi luzes, um programador e cabos necessários para programar e ligar os dispositivos.



# Hardware

Cada um dos  módulos que constituem este kit são incorporam duas placas, um placa PCB (wifi board) dedicada ao processamento, controlo e comunicação e outra placa PCB (baseboard) dedicada à interface física IO e à regulação de energia.



## WIFI board

Esta placa integra um modulo wifi ESP12F baseado no esp8266.Nesta também está o regulador de tensão DC-DC responsável por converter 5V para 3.3V, tal como está incluído a proteção e interface elétrica das saídas e das entradas digitais e analógica. Esta placa é utilizada nas varias versões do modulo sendo alterada apenas a inclusão das resistências R7 e R8, caso seja pretendida uma entrada analógica insere-se a resistência R8 para utilizar a entrada digital 2 insere-se R7.



![base_board_outlet](https://bitbucket.org/atouchinter/bragaiotchallenge/raw/98a761aefab549cbc62ada741b5a56d47baae101/imagens/wifi_hw.JPG)

| Função            | GPIO |
| ----------------- | ---- |
| Entrada digital 1 | 13   |
| Entrada digital 2 | 12   |
| Entrada analógica | ADC  |
| Saída digital 1   | 14   |
| Saída digital 2   | 4    |



## Outlet baseboard

![base_board_outlet](https://bitbucket.org/atouchinter/bragaiotchallenge/raw/98a761aefab549cbc62ada741b5a56d47baae101/imagens/base_board_outlet.JPG)



Esta placa, junto com a wifi board constitui o modulo de tomadas. Esta é responsável pela conversão CA/CC de tensão de alimentação 230VAC para 5VDC, permitir a interface física à instalação através da ligação de fios e tem o relé de 16A de atuação. 

## Light baseboard

![base_board_light](https://bitbucket.org/atouchinter/bragaiotchallenge/raw/98a761aefab549cbc62ada741b5a56d47baae101/imagens/base_board_light.JPG)

Esta placa, junto com a wifi board constitui o modulo de luzes. Esta é responsável pela conversão CA/CC de tensão de alimentação 230VAC para 5VDC, permitir a interface física à instalação através da ligação de fios e tem os dois relés de 10A de atuação. 

